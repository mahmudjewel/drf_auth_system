﻿## Authentication System
This is an authentication systen where users can create profile, login, logout, update password and reset password with email.

## ROUTES
| METHOD | ROUTE | FUNCTIONALITY |ACCESS|
| ------- | ----- | ------------- | ------------- |
| *POST* | ```/auth/users/``` | _Create new user account_| _All users_|
| *POST* | ```/auth/users/activation/``` | _Activate user_|_Authenticated_|
| *POST* | ```/auth/users/resend_activation/``` | _Resend Activation E-mail_|_Authenticated_|
| *POST* | ```/auth/users/reset_password/``` | _Forget/Reset Password_|_Authenticated_|
| *POST* | ```/auth/users/reset_password_confirm/``` | _Forget/Reset Password Confirmation_|_Authenticated_|
| *POST* | ```/api/auth/jwt/create``` | _Login user_|_All users_|
| *POST* | ```/api/auth/jwt/refresh/``` | _Refresh the access token_|_All users_|
| *POST* | ```/api/auth/jwt/verify/``` | _Verify the validity of a token_|_All users_|

## How to run the Project
- Git clone the project with ```https://gitlab.com/bullet007/drf_auth_system.git```
- Create your virtualenv and activate it.
- Install the requirements with ``` pip install -r requirements.txt ```
- Create you database with `python manage.py runserver` 
- Finally run the API 
``` python manage.py runserver ``

