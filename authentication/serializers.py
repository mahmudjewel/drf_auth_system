from djoser.serializers import UserCreateSerializer
from django.contrib.auth import get_user_model
User = get_user_model()

class UserCreateSerializer(UserCreateSerializer):
	class Meta(UserCreateSerializer.Meta):
		model = User
		fields = ('id', 'email', 'first_name', 'last_name', 'password')
		
		# overwrite create for set password 
	def create(self, validated_data):
		user = super(UserCreateSerializer, self).create(validated_data)
		user.set_password(validated_data['password'])
		user.save()
		return user
